from typing import Any

from airflow.models.baseoperator import BaseOperator
from hooks.pygsheets_hook import PygsheetsHook


class PygsheetsOperator(BaseOperator):

    def __init__(self, share_email, spreadsheet_name, **kwargs) -> None:
        super().__init__(**kwargs)
        self.share_email = share_email
        self.spreadsheet_name = spreadsheet_name

    def execute(self, context: Any):
        hook = PygsheetsHook(share_email=self.share_email)
        spreadsheet = hook.get_or_create_spreadsheet(spreadsheet_name=self.spreadsheet_name)
        return spreadsheet
