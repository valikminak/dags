from airflow.hooks.base import BaseHook
from pygsheets import Cell, DataRange
from pygsheets.exceptions import SpreadsheetNotFound, WorksheetNotFound
import pygsheets


class PygsheetsHook(BaseHook):
    def __init__(self, share_email):
        super().__init__()
        self.share_email = share_email

    @staticmethod
    def _has_perm(permissions: dict, email: str) -> bool:
        return any(x['emailAddress'] == email for x in permissions)

    def get_or_create_spreadsheet(self, spreadsheet_name):
        gsheet_authorized = pygsheets.authorize(service_file='/opt/bitnami/airflow/creds/gsheets_creds.json')
        try:
            spreadsheet = gsheet_authorized.open(spreadsheet_name)
        except SpreadsheetNotFound:
            spreadsheet = gsheet_authorized.create(spreadsheet_name)
        if not self._has_perm(spreadsheet.permissions, self.share_email):
            spreadsheet.share(self.share_email, role='writer')

        return spreadsheet

    @staticmethod
    def add_worksheet(spreadsheet, worksheet_name, wks_header, data_matrix):
        try:
            wks = spreadsheet.worksheet_by_title(worksheet_name)
            wks.clear()
        except WorksheetNotFound:
            wks = spreadsheet.add_worksheet(worksheet_name, index=0)

        ws_titles = [wks_header, *data_matrix]
        wks.update_values('A1', ws_titles)
        return wks

    @staticmethod
    def abuse_report_worksheet_style(worksheet, last_row_coords):
        # format cells - font size, colors, font weight
        model_cell = Cell('A1')
        model_cell.text_format['fontSize'] = 8
        DataRange('A1', f'J{last_row_coords}', worksheet=worksheet).apply_format(model_cell)
        model_cell.color = (0.6901, 0.7019, 0.698, 1.0)  # header color grey
        model_cell.text_format['bold'] = True
        DataRange('A1', 'J1', worksheet=worksheet).apply_format(model_cell)
        model_cell.color = (0.8313, 0.8313, 0.8313, 1.0)  # lighter than header
        DataRange('A2', f'A{last_row_coords}', worksheet=worksheet).apply_format(model_cell)
