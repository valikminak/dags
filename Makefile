run:
	AIRFLOW__CORE__EXECUTOR=DebugExecutor poetry run python bundleCache.py

init:
	mkdir ./dags ./logs ./plugins
	echo -e "AIRFLOW_UID=1000\nAIRFLOW_GID=0" > .env
	poetry run docker-compose up airflow-init

	#AIRFLOW_HOME=./airflow airflow db init
up:
	poetry run docker-compose up 

