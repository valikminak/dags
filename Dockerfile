FROM apache/airflow:2.5.0-python3.9

RUN pip install --upgrade pip
RUN pip install -U airflow_clickhouse_plugin && pip install -U pygsheets
