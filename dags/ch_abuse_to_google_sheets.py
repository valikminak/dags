from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator
from airflow_clickhouse_plugin.operators.clickhouse_operator import ClickHouseOperator
from airflow.models import Variable
import datetime
from airflow.providers.slack.hooks.slack_webhook import SlackWebhookHook

from plugins.hooks.pygsheets_hook import PygsheetsHook

SHARE_EMAIL = Variable.get("share_email", "valik@esmedia.es")
to_date = datetime.date.today()
from_date = to_date - datetime.timedelta(days=7)
ch_from_date = int(from_date.strftime("%Y%m%d").replace('/', ''))
ch_to_date = int(to_date.strftime("%Y%m%d").replace('/', ''))

query = """
SELECT DISTINCT 
       toString(hid_ts)                   as hid_ts,
       profiles.username                  as affiliate,
       hid_profile_id,
       ams.username                       as manager,
       JSONExtractString(form, 'email')   as email,
       JSONExtractString(form, 'message') as message,
       locator_remote                     as ip,
       hid_str
FROM mq.log any
         left join (select user_id, username, affiliate_manager_id from sitedb.profiles_profile) profiles
                   on hid_profile_id = toUInt32(profiles.user_id)
            any
         left join (select user_id, username, affiliate_manager_id from sitedb.profiles_profile) ams
                   on profiles.affiliate_manager_id = ams.user_id
WHERE toYYYYMMDD(date) BETWEEN {} AND {} AND location = 'abuse' 
ORDER BY timestamp desc
""".format(ch_from_date, ch_to_date)


def send_link_to_slack(ti):
    ws_link = ti.xcom_pull('add_ws_with_ch_data')
    hook = SlackWebhookHook(
        http_conn_id='slack_webhook',
        message="<{}|Abuse report {} - {}>".format(ws_link, from_date.strftime('%d.%m'),
                                                   to_date.strftime('%d.%m')),
        username='Airflow',
        channel="#team_sup"
    )
    hook.execute()


def add_worksheet_to_abuse_report_spreadsheet(**context):
    data_matrix = context['ti'].xcom_pull("fetch_abuse_data")
    wks_name = f"{from_date.strftime('%d.%m')}-{to_date.strftime('%d.%m')}"
    wks_header = ["date", "affiliate", "hid_profile_id", "manager", "email", "message", "ip", "hid_str"]

    pygsheets_hook = PygsheetsHook(share_email=SHARE_EMAIL)
    spreadsheet = pygsheets_hook.get_or_create_spreadsheet(spreadsheet_name='Abuse_grafana_data_export')
    worksheet = pygsheets_hook.add_worksheet(
        spreadsheet=spreadsheet,
        worksheet_name=wks_name,
        wks_header=wks_header,
        data_matrix=data_matrix
    )
    pygsheets_hook.abuse_report_worksheet_style(worksheet=worksheet, last_row_coords=len(data_matrix) + 1)
    return worksheet.url


with DAG(
        'ch_abuse_report',
        start_date=days_ago(7),
        schedule_interval="0 0 * * MON",  # every monday
) as dag:
    ClickHouseOperator(
        task_id='fetch_abuse_data',
        database='default',
        sql=query,
        clickhouse_conn_id='ch_abuse',
    )
