import base64
import json
import logging
import os
import re

import requests
from airflow import DAG, XComArg
from airflow.decorators import task
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.utils.dates import days_ago
from requests.adapters import HTTPAdapter, Retry
from requests.auth import AuthBase
from airflow.providers.slack.hooks.slack_webhook import SlackWebhookHook

MAGISTRA_URL = os.getenv('MAGISTRA_URL', 'http://127.0.0.1:8000/')
LINKS = [
    'https://raw.githubusercontent.com/easylist/easylist/master/easylist/easylist_adservers.txt',
    'https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt',
    'https://cdn.cliqz.com/adblocker/resources/plowe-0/499b1f04663286d1f5a869295f8032e98af37aeb041e6a0ac2005a97f04dd65e/list.txt',
    'https://cdn.cliqz.com/adblocker/resources/ublock-badware/25d83f8745b2a4ce741c9c78ecb60b73f39a54381e204ececa809f7cd2ae2120/list.txt',
]

retry_strategy = Retry(
    total=3,
    status_forcelist=[500, 502, 503, 504],
    method_whitelist=["GET"],
    backoff_factor=1
)
adapter = HTTPAdapter(max_retries=retry_strategy)

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class BasicAuth(AuthBase):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __call__(self, r):
        creds = self.username + ":" + self.password
        r.headers['Authorization'] = 'Basic ' + base64.b64encode(creds.encode("ascii")).decode("ascii")
        return r


with DAG(
        dag_id="adblock_checker",
        start_date=days_ago(1),
        schedule_interval="0 0 * * *"  # every day at 00:00
) as dag:
    get_magistra_domains = SimpleHttpOperator(
        task_id="get_magistra_domains",
        http_conn_id="adblock_checker_basicauth",
        method="GET",
        endpoint="api-adblock-checker/",
        auth_type=BasicAuth,
        response_filter=lambda response: json.loads(response.text)
    )


    @task
    def check_if_magistra_domains_in_blocklist(link, magistra_domains):
        pattern = '[A-Za-z0-9-]+[.]com|com\^'
        request = requests.Session()
        request.mount('https://', adapter)
        response = request.get(link)
        domain_list = getattr(response, 'content').decode('utf-8')
        response_domains = {domain.replace('||', '') for domain in re.findall(pattern, domain_list)}
        logger.info("-|- Got %s domains from %s:", len(response_domains), link)
        logger.info(response_domains)
        logger.info("-|- Let's check if any of the magistra domains is in the blocklist")
        blocked_domains = {}
        for domain in response_domains:
            domain_pk = magistra_domains.get(domain)
            if domain_pk:
                blocked_domains[domain] = domain_pk
        return blocked_domains


    def send_message(ti):
        blocked_domains = ti.xcom_pull('distinct_blocked_domains')

        message_text = '<Adblock-checker> has detected blocked domains: ' + \
                       ' '.join(['<{}host_config/domain/{}/change/|{}>;'
                                .format(MAGISTRA_URL, d_pk, d_d) for d_d, d_pk in blocked_domains.items()])
        hook = SlackWebhookHook(
            http_conn_id='slack_webhook',
            message=message_text,
            username='Airflow',
            channel="#k8s-domains"
        )
        hook.execute()


    def distinct(ti):
        blocked_domains = ti.xcom_pull('check_if_magistra_domains_in_blocklist')
        distinct_domains = {}
        for x in list(blocked_domains):  # [{domain1: pk, domain2: pk}, {...} ...]
            for domain, pk in x.items():
                distinct_domains[domain] = pk
        return distinct_domains


    distinct_blocked_domains = PythonOperator(
        task_id="distinct_blocked_domains",
        python_callable=distinct,
    )

    send_message_to_slack = PythonOperator(
        task_id="send_message_to_slack",
        python_callable=send_message
    )

    no_blocked_domains = BashOperator(
        task_id="no_blocked_domains",
        bash_command="echo There were no found blocked domains"
    )

    check = check_if_magistra_domains_in_blocklist \
        .partial(magistra_domains=XComArg(get_magistra_domains)) \
        .expand(link=LINKS)

    handle_results = BranchPythonOperator(
        task_id="handle_results",
        python_callable=lambda ti: ["distinct_blocked_domains", "send_message_to_slack"]
        if any(ti.xcom_pull("check_if_magistra_domains_in_blocklist"))
        else ["no_blocked_domains"],
    )

    check >> handle_results >> distinct_blocked_domains >> send_message_to_slack
    check >> handle_results >> no_blocked_domains
