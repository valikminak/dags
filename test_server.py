import json
import socket

server = socket.create_server(("127.0.0.1", 8000))
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server.listen(10)

m = {"id": 2, "name": "abc"}
checker = {"id": 2, "name": "{domain: 232332323}"}


try:
    client_socket, address = server.accept()
    received_data = client_socket.recv(1024).decode('utf-8')
    path = received_data.split(" ")[1]
    if path == "/adblock":
        data = json.dumps(checker)
        client_socket.sendall(bytes(data, encoding="utf-8"))
    data = json.dumps(m)
    client_socket.sendall(bytes(data, encoding="utf-8"))
finally:
    client_socket.close()
# except KeyboardInterrupt:
#     server.shutdown(socket.SHUT_RDWR)
#     server.close()

# !/usr/bin/env python3

